﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class InformationMessageWindow : MonoBehaviour {

    public Text messageText;
    public GameObject messageBox;

    void Start()
    {
        messageBox.SetActive(false);
    }

    public void DisplayMessage(string message)
    {
        messageText.text = message;

        StartCoroutine(PresentMessageWindow());
    }

    private IEnumerator PresentMessageWindow()
    {
        messageBox.SetActive(true);
        yield return new WaitForSeconds(3);
        messageBox.SetActive(false);
    }
}