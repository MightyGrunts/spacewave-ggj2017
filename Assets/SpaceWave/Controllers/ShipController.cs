﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ShipController : MonoBehaviour {

    public Text shipLocation;
    public Text shipHp;
    public Text shipEnergy;
    public Text shipClass;

    GameController gameController;
    Ship ship;

	void Awake()
    {
        gameController = FindObjectOfType<GameController>();
    }

	void Start () {
        gameController.ShipDataRecieved += OnShipDataRecieved;
    }

    private void OnShipDataRecieved(Ship ship)
    {
        Debug.LogError("Ship heard Ship data recieved");
        this.ship = ship;
        shipLocation.text = "Zone: " + ship.currentZoneName;
        shipHp.text = ship.hitPoints + " HP";
        shipEnergy.text = "Energy: " + ship.energy;
        shipClass.text = "Ship Class: " + ship.shipClass;
    }

    public void MoveToZone(int zoneId)
    {

    }
}
