﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    public Action<Ship> ShipDataRecieved;

    string email;
    string passwordHash;
    string privateMessageChannel;
    string zoneDataChannel;
    string shipDataChannel;

    Ship ship; 

    InformationMessageWindow informationMessageWindow;
    NetworkMessageController networkMessageController;

    void Awake () {
        networkMessageController = FindObjectOfType<NetworkMessageController>();
        informationMessageWindow = FindObjectOfType<InformationMessageWindow>();

        email = PlayerPrefs.GetString("email");
        passwordHash = PlayerPrefs.GetString("passwordHash");
        privateMessageChannel = email;

        shipDataChannel = "Ship_" + email;
        zoneDataChannel = "Zone_" + email;
    }

    void Start()
    {
        networkMessageController.SubscribeTochannel(shipDataChannel, onShipDataRecieved);
        networkMessageController.SubscribeTochannel(zoneDataChannel, onZoneDataRecieved);

        RequestShipData();
        RequestZoneData();
    }

    private void onZoneDataRecieved(string obj)
    {
        Debug.LogError("ZONE DATA RECIEVED!");
        Debug.LogError("Private Zone data recieved: " + obj);
        string json = networkMessageController.GetJsonStringFromMessageData(obj);
        Debug.LogError("Scrubbed: " + json);
    }

    private void onShipDataRecieved(string obj)
    {
        Debug.LogError("Private Ship data recieved: " + obj);
        string json = networkMessageController.GetJsonStringFromMessageData(obj);
        Debug.LogError("Scrubbed: " + json);

        Ship ship = JsonUtility.FromJson<Ship>(json);
        Debug.LogError("Player is using a " + ship.shipClass + " in zone " + ship.currentZoneId);

        if(ShipDataRecieved != null)
        {
            ShipDataRecieved(ship);
        }
    }

    public void RequestZoneData(int zoneId = 1)
    {
        string ZoneRequest = string.Format("{{\"returnChannel\": \"{0}\", \"zoneId\": \"{1}\"}}", privateMessageChannel, zoneId);
        Debug.LogError("ZoneRequest: " + ZoneRequest);
        networkMessageController.Publish("RequestZoneData", ZoneRequest, HandlePublishMessage);
    }

    public void RequestShipData()
    {
        string ShipDataRequest = string.Format("{{\"returnChannel\": \"{0}\", \"email\": \"{1}\"}}", privateMessageChannel, email);
        Debug.LogError("ShipRequest: " + ShipDataRequest);
        networkMessageController.Publish("RequestShipData", ShipDataRequest, HandlePublishMessage);
    }

    private void HandlePublishMessage(string obj)
    {
        //
    }

    public void MoveToRandomZone()
    {
        int newZoneId = UnityEngine.Random.Range(1, 31);
        //int currentZone = ship.currentZoneId;
        string MoveShipRequest = string.Format("{{\"returnChannel\": \"{0}\", \"zoneId\": \"{1}\"}}", privateMessageChannel, newZoneId);
        networkMessageController.Publish("MoveShip", MoveShipRequest, HandlePublishMessage);
    }
}
