﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;

public class LoginController : MonoBehaviour {
    public InputField emailAddress;
    public InputField password;
    public Button loginButton;

    Action<string> loginStatus;
    string uniqueId;

    public void Start()
    {
        loginButton.onClick.AddListener(SendCredentials);

        uniqueId = System.Guid.NewGuid().ToString();
        loginStatus += processLoginStatus;

        NetworkMessageController.instance.SubscribeTochannel(uniqueId, loginStatus);
    }

    private void processLoginStatus(string status)
    {
        Debug.LogError("Stattus: " + status);
        LoginResult loginResult;

        if (!string.IsNullOrEmpty(status) && !string.IsNullOrEmpty(status.Trim()))
        {
            List<object> deserializedMessage = NetworkMessageController.instance.pubnub.JsonPluggableLibrary.DeserializeToListOfObject(status);
            if (deserializedMessage != null && deserializedMessage.Count > 0)
            {
                object subscribedObject = (object)deserializedMessage[0];
                if (subscribedObject != null)
                {
                    loginResult = JsonUtility.FromJson<LoginResult>(subscribedObject.ToString());
                    switch (loginResult.status)
                    {
                        case "SUCCESS":
                            HandleLogin();
                            break;
                        case "INCORRECT_PASSWORD":
                            HandleIncorrectPassword();
                            break;
                        case "NEW_USER":
                            HandleNewUser();
                            break;
                    }
                }
            }
        }
    }

    private void HandleNewUser()
    {
        //UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
        this.SendCredentials();
    }

    private void HandleIncorrectPassword()
    {
        Debug.Log("[ Login ] Password incorrect");
    }

    private void HandleLogin()
    {
        Debug.Log("[ Login ] Loading Game");
        //UnityEngine.SceneManagement.SceneManager.LoadScene("NewMain");
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }

    public const string MatchEmailPattern =
            @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
              + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    public void SendCredentials()
    {
        if (IsEmail(emailAddress.text) && !string.IsNullOrEmpty(password.text))
        {
            string passwordHash = ComputeHash(password.text);
            string jsonMessage = "{\"username\": \"" + emailAddress.text + "\", \"password\": \"" + passwordHash + "\",  \"privateChannel\": \"" + uniqueId + "\"}";
            PlayerPrefs.SetString("email", emailAddress.text);
            PlayerPrefs.SetString("passwordHash", passwordHash);
            PlayerPrefs.SetString("privateChannel", uniqueId);
            //Debug.LogError("[ Message ] " + jsonMessage);
            NetworkMessageController.instance.Publish("PlayerLogin", jsonMessage, processLoginStatus);

        }
        else
        {
            Debug.LogError("Entered Invalid email address or blank password");
        }
    }

    public static bool IsEmail(string email)
    {
        if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
        else return false;
    }

    public static string ComputeHash(string s)
    {
        // Form hash
        System.Security.Cryptography.MD5 h = System.Security.Cryptography.MD5.Create();
        byte[] data = h.ComputeHash(System.Text.Encoding.Default.GetBytes(s));
        // Create string representation
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < data.Length; ++i)
        {
            sb.Append(data[i].ToString("x2"));
        }
        return sb.ToString();
    }
}

class LoginResult
{
    public string status;
}