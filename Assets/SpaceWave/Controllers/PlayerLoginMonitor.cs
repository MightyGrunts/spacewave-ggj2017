﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PlayerLoginMonitor : MonoBehaviour {

    string email;
    string passwordHash;
    InformationMessageWindow informationMessageWindow;

    void Start()
    {
        informationMessageWindow = FindObjectOfType<InformationMessageWindow>();

        email = PlayerPrefs.GetString("email");
        passwordHash = PlayerPrefs.GetString("passwordHash");

        NetworkMessageController.instance.SubscribeTochannel("PlayerLoggedIn", onPlayerLoggedIn);

        Debug.Log("[ GameController ]  Starting");
        if (!string.IsNullOrEmpty(email))
        {
            NetworkMessageController.instance.Publish("PlayerLoggedIn", email, onPlayerLoggedIn);
        }
        else
        {
            informationMessageWindow.DisplayMessage(string.Format("Invalid something"));
        }
    }

    private void onPlayerLoggedIn(string playerData)
    {
        if (!string.IsNullOrEmpty(playerData) && !string.IsNullOrEmpty(playerData.Trim()))
        {
            List<object> deserializedMessage = NetworkMessageController.instance.pubnub.JsonPluggableLibrary.DeserializeToListOfObject(playerData);
            if (deserializedMessage != null && deserializedMessage.Count > 0)
            {
                object subscribedObject = (object)deserializedMessage[0];

                if (subscribedObject != null)
                {
                    string emailAddress = subscribedObject.ToString();
                    informationMessageWindow.DisplayMessage(string.Format("{0} logged in", emailAddress));
                }
            }
        }
    }
}
