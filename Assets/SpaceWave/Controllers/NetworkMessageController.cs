﻿using UnityEngine;
using System.Collections;
using PubNubMessaging.Core;
using System;
using System.Collections.Generic;

public class NetworkMessageController : MonoBehaviour
{

    public static NetworkMessageController instance;
    public Pubnub pubnub;

    public Action<string> connectCallback;
    public Action<PubnubClientError> errorCallback;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        connectCallback += DisplaySubscribeConnectStatusMessage;
        errorCallback += DisplayErrorMessage;

        pubnub = new Pubnub("pub-c-4f006623-3db4-4b20-a24c-b5cee7f65455", "sub-c-5dbf2350-4a1a-11e6-9baf-0619f8945a4f");
        pubnub.Subscribe<string>("SpaceWave", DisplaySubscribeReturnMessage, connectCallback, errorCallback);

    }

    public void Publish(string channel, string message, Action<string> callback)
    {
        pubnub.Publish(channel, message, callback, errorCallback);
    }

    public void SubscribeTochannel(string channel, Action<string> callback)
    {
        pubnub.Subscribe<string>(channel, callback, connectCallback, errorCallback);
    }

    private void DisplayErrorMessage(PubnubClientError obj)
    {
        //Debug.LogWarningFormat("[ PubNub ] Error: {1} : {2} : {3} : {4} ", obj.Channel, obj.Description, obj.Message, obj.Severity);
        Debug.LogError("PUB ERROR: " + obj);
    }


    private void DisplaySubscribeConnectStatusMessage(string obj)
    {
        Debug.LogWarningFormat("[ PubNub ] Connection Status Message: {0}", obj);
    }


    private void DisplaySubscribeReturnMessage(string obj)
    {
        Debug.LogWarningFormat("[ PubNub ] Subscribe Return Message: {0}", obj);
    }

    public string GetJsonStringFromMessageData(string messageData)
    {
        if (!string.IsNullOrEmpty(messageData) && !string.IsNullOrEmpty(messageData.Trim()))
        {
            List<object> deserializedMessage = NetworkMessageController.instance.pubnub.JsonPluggableLibrary.DeserializeToListOfObject(messageData);
            if (deserializedMessage != null && deserializedMessage.Count > 0)
            {
                object subscribedObject = (object)deserializedMessage[0];

                if (subscribedObject != null)
                {
                    return subscribedObject.ToString();
                }
            }
        }

        return "";
    }
}